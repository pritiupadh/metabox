package com.proj.metabox;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;
import android.R.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ListActivity {
	static final String FTP_HOST= "192.168.0.103";
    static final String FTP_USER = "admin";
    static final String FTP_PASS  ="admin";
	private Button up;
    String ss=null;
	private File currentDir;
    private FileArrayAdapter adapter;
    @SuppressLint("SdCardPath")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        currentDir = new File("/sdcard/");
        fill(currentDir);
        up=(Button)findViewById(R.id.upButton);
        Button trial=(Button)findViewById(R.id.btntrial);
        Log.e("this",trial.toString());
        up.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				File f = new File("/sdcard/"+ss);
		        uploadFile(f);
			}
		});
        
        
        Log.e("this","here1");
    }
    public void uploadFile(File fileName){
        
        
        FTPClient client = new FTPClient();
         
       try {
            
           client.connect(FTP_HOST,21);
           client.login(FTP_USER, FTP_PASS);
           client.setType(FTPClient.TYPE_BINARY);
           client.changeDirectory("/upload/");
            
           client.upload(fileName, new MyTransferListener());
            
       } catch (Exception e) {
           e.printStackTrace();
           try {
               client.disconnect(true);    
           } catch (Exception e2) {
               e2.printStackTrace();
           }
       }
        
   }
    public class MyTransferListener implements FTPDataTransferListener {
    	 
        public void started() {
             
            up.setVisibility(View.GONE);
            // Transfer started
            Toast.makeText(getBaseContext(), " Upload Started ...", Toast.LENGTH_SHORT).show();
            //System.out.println(" Upload Started ...");
        }
 
        public void transferred(int length) {
             
            // Yet other length bytes has been transferred since the last time this
            // method was called
            Toast.makeText(getBaseContext(), " transferred ..." + length, Toast.LENGTH_SHORT).show();
            //System.out.println(" transferred ..." + length);
        }
 
        public void completed() {
             
            up.setVisibility(View.VISIBLE);
            // Transfer completed
             
            Toast.makeText(getBaseContext(), " completed ...", Toast.LENGTH_SHORT).show();
            //System.out.println(" completed ..." );
        }
 
        public void aborted() {
             
            up.setVisibility(View.VISIBLE);
            // Transfer aborted
            Toast.makeText(getBaseContext()," transfer aborted , please try again...", Toast.LENGTH_SHORT).show();
            //System.out.println(" aborted ..." );
        }
 
        public void failed() {
             
            up.setVisibility(View.VISIBLE);
            // Transfer failed
            System.out.println(" failed ..." );
        }
 
    }

    private void fill(File f)
    {
        File[]dirs = f.listFiles();
         this.setTitle("Current Dir: "+f.getName());
         List<option>dir = new ArrayList<option>();
         List<option>fls = new ArrayList<option>();
         try{
             for(File ff: dirs)
             {
                if(ff.isDirectory())
                    dir.add(new option(ff.getName(),"Folder",ff.getAbsolutePath()));
                else
                {
                    fls.add(new option(ff.getName(),"File Size: "+ff.length(),ff.getAbsolutePath()));
                }
             }
         }catch(Exception e)
         {
             
         }
         Collections.sort(dir);
         Collections.sort(fls);
         dir.addAll(fls);
         if(!f.getName().equalsIgnoreCase("sdcard"))
             dir.add(0,new option("..","Parent Directory",f.getParent()));
         adapter = new FileArrayAdapter(MainActivity.this,R.layout.file_view,dir);
         //ListView lv = (ListView)findViewById(android.R.id.list);
         setListAdapter(adapter);
    }
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        // TODO Auto-generated method stub
        super.onListItemClick(l, v, position, id);
        option o = adapter.getItem(position);
        if(o.getData().equalsIgnoreCase("folder")||o.getData().equalsIgnoreCase("parent directory")){
                currentDir = new File(o.getPath());
                fill(currentDir);
        }
        else
        {
            onFileClick(o);
        }
    }
    private void onFileClick(option o)
    {
        Toast.makeText(this, "File Clicked: "+o.getName(), Toast.LENGTH_SHORT).show();
        ss=o.getName();
        Toast.makeText(getApplicationContext(), ss, Toast.LENGTH_SHORT).show();
    }
}